import pandas as pd
import uuid,sys

#讲师名称（Excel中字段名）
name = "张洁（西安）"
#导出课表的月份
month = "2021-1"
#输出文件名
output = name + month + '课程安排.ics'

if len(sys.argv) < 2:
    print("请指定Excel文件")
    sys.exit()

cal = pd.read_excel(sys.argv[1], sheet_name=0, skiprows=1)
#选择列
cal = cal[["讲师", name]].set_index('讲师')
courses = pd.Series(cal[name], index=cal.index)
#删除无安排的日期
courses = courses[courses.notnull()]

with open(output, 'w', encoding = 'utf-8') as f:
    f.write("BEGIN:VCALENDAR\n")
    f.write("PRODID:-//HQYJ//Curriculum//EN\n")
    f.write("VERSION:2.0\n")
    f.write("METHOD:PUBLISH\n")

    #遍历2021年1月的课程安排
    for date, course in courses['2021-1'].items():
        f.write("BEGIN:VEVENT\n")
        f.write("DTSTART;VALUE=DATE:%s\n" % str(date.strftime("%Y%m%d")))
        f.write("SUMMARY:%s\n" % course)
        f.write("UID:%s\n" % str(uuid.uuid1()))
        f.write("TRANSP:TRANSPARENT\n")
        f.write("END:VEVENT\n")

    f.write("END:VCALENDAR\n")
