import pandas as pd
import sys

output = '员工通讯录.vcf'

if len(sys.argv) < 2:
    print("请指定Excel文件")
    sys.exit()

contacts = pd.read_excel(sys.argv[1], engine="openpyxl",sheet_name=0, skiprows=1)

with open(output, 'w', encoding = 'utf-8') as f:
    #将每一行转为字典
    for contact in contacts.to_dict("records"):
        f.write("BEGIN:VCARD\n")
        f.write("VERSION:3.0\n")
        f.write("N:" + contact["姓名"][0] + ";" + contact["姓名"][1:] + ";;;\n")
        f.write("FN:%s\n" % contact["姓名"])
        f.write("ORG;CHARSET=UTF-8:华清远见\n")
        f.write("TITLE:%s\n" % contact["职务"])
        f.write("TEL;TYPE=WORK:%s\n" % contact["联系电话"])
        f.write("EMAIL:%s\n" % contact["邮箱"])
        #f.write("X-QQ:%.0f\n" % contact["QQ"])
        f.write("END:VCARD\n")
